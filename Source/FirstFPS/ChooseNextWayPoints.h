// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "ChooseNextWayPoints.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTFPS_API UChooseNextWayPoints : public UBTTaskNode
{
	GENERATED_BODY()


		virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere, Category = "Blackboard")
		struct FBlackboardKeySelector IndexKey;//Index of way point(0,1,2,3..)
	
	UPROPERTY(EditAnywhere, Category = "Blackboard")
		struct FBlackboardKeySelector wayPointKey;//Name of the way point(Upstairs, target1, target2...)

};

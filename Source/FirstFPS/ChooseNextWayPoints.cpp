// Fill out your copyright notice in the Description page of Project Settings.

#include "ChooseNextWayPoints.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "PatrolRoute.h"
#include "AIController.h"//allow to use behavior tree

//APatrollingGuard PG;

EBTNodeResult::Type UChooseNextWayPoints::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
//Get the PatrolRoute component
	auto conrolledPawn = OwnerComp.GetAIOwner()->GetPawn();//pawn who`s manage by the behavior tree
	auto patrollRoute = conrolledPawn->FindComponentByClass<UPatrolRoute>();//access to patrolRout class(component)
	//protect agains empty patroul component;
	if (!ensure(patrollRoute))
	{
		return EBTNodeResult::Failed;
	}
	
//Get the massive of target points from PatrolRoute
	TArray<AActor*> patrollPoints = patrollRoute->GetPatrolPoints();
	
	//if array is empty
	if (!patrollPoints.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("A guard is missing patrol points"));
		return EBTNodeResult::Failed;
	}

//Set Next Index
	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent() ;// get a blackboard
	int Index = blackboardComp->GetValueAsInt(IndexKey.SelectedKeyName);//get the number of index key
	blackboardComp->SetValueAsObject(wayPointKey.SelectedKeyName, patrollPoints[Index]);//change way point name by the index

//Cycle Index
	int nextIndex = (Index + 1) % patrollPoints.Num();
	blackboardComp->SetValueAsInt(IndexKey.SelectedKeyName, nextIndex);
	return EBTNodeResult::Succeeded;
}



